import unittest

from FlynnClient.core.error import get_request_ekey
from FlynnClient.utils.crypto import aes_decrypt
from FlynnClient.utils.string import str_to_bytes
from tests._core import BaseTest


class TestPing(BaseTest):

    def test_ping(self):
        response = self.clientCtxt.client.ping()

        self.assertIsNotNone(response, "err-call-to-server-failed")

        response_data = response.json()
        self.assertIsInstance(response_data, dict)

        if response.status_code == 200:
            self.assertEqual(response_data['data']['message'], "pong", "err-unexpected-server-response")
        else:
            self.fail(response_data['error']['message'])

    def test_authenticated_api_call(self):
        response = self.clientCtxt.client.authenticate()

        self.assertIsNotNone(response, "err-call-to-server-failed")

        response_data = response.json()
        self.assertIsInstance(response_data, dict)

        if response.status_code == 200:
            self.assertEqual(response_data['data']['message'], "pong", "err-unexpected-server-response")
        else:
            self.fail(response_data['error']['message'])

    def test_authenticated_api_call_with_sensitive_data(self):
        rek = get_request_ekey()  # generate request encryption key
        client_message = "some-secret-message"

        # calling the API
        response = self.clientCtxt.client.authentication_with_sensitive_data(rek, client_message)

        self.assertIsNotNone(response, "err-call-to-server-failed")

        # parsing the response
        response_data = response.json()
        self.assertIsInstance(response_data, dict)

        if response.status_code == 200:
            self.assertEqual(response_data['data']['message'], "pong", "err-unexpected-server-response")

            secret_data = response_data['data']["client-message"]
            decrypted_secret_data = aes_decrypt(str_to_bytes(rek), secret_data)
            self.assertEqual(decrypted_secret_data, client_message, "err-invalid-client-message")
        else:
            self.fail(response_data['error']['message'])


if __name__ == '__main__':
    unittest.main()
