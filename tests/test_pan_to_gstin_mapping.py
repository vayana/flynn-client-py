import json
import unittest

from FlynnClient.core.constants import SEP
from FlynnClient.utils.serialization import print_pretty
from tests._core import BaseTest

PAN, GSTIN = "GSPTN0791G", "33GSPTN0791G1Z5"  # SANDBOX


class TestPANToGSTINMapping(BaseTest):

    def __call_api(self):
        flynn_version = "v1.0"

        response = self.clientCtxt.client.get(f"/enriched/gstn/{flynn_version}/search-taxpayers-from-pan/{PAN}")
        self.assertIsNotNone(response, "err-call-to-server-failed")
        response_data = response.json()
        self.assertIsInstance(response_data, dict)

        if response.status_code == 200:
            task_reference = response_data["data"]["task-id"]
        else:
            self.fail(response_data['error']['message'])

        return task_reference

    def test_pan_to_gstin_mapping(self):
        # initially we call the actual API
        # we will get async task id back
        task_id = self.__call_api()

        self._wait_for_task_to_complete(task_id)  # implements `status` API
        gstin_info_map = self._call_result_api(task_id)

        self.assertEqual(1, len(gstin_info_map))
        self.assertIsNotNone(gstin_info_map.get(GSTIN))
        search_tp_data = gstin_info_map.get(GSTIN)

        print(f"\n{SEP}\nSearch Taxpayer Response:\n")
        print_pretty(json.loads(search_tp_data))
        return


if __name__ == '__main__':
    unittest.main()
