import unittest

from FlynnClient.core.constants import SEP
from tests._core import BaseTest


class TestBasicGSTPublicAPI(BaseTest):

    def test_search_taxpayer(self):
        gstn_api_version, gstin = "v0.3", "33GSPTN0791G1Z5"  # SANDBOX
        uri = f"/basic/gstn/v1.0/commonapi/{gstn_api_version}/search/{gstin}"
        response = self.clientCtxt.client.get(uri)
        result = self._parse_json_response(response)
        print(f"{SEP}Payload:\n{result}")
        return

    def test_view_and_track_returns(self):
        gstn_api_version, gstin = "v1.0", "33GSPTN0791G1Z5"  # SANDBOX
        uri = f"/basic/gstn/v1.0/commonapi/{gstn_api_version}/returns/{gstin}?fy=2017-18"
        response = self.clientCtxt.client.get(uri)
        result = self._parse_json_response(response)
        print(f"{SEP}Payload:\n{result}")
        return


if __name__ == '__main__':
    unittest.main()
