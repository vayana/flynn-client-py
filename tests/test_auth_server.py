import traceback
import unittest

from FlynnClient.auth import login_and_get_auth_token
from FlynnClient.core.loader import get_transient_root, load_config
from FlynnClient.handler import FilePersistenceHandler
from tests._core import PROJECT_ROOT


class TestAuthServer(unittest.TestCase):
    config = None
    handler = None

    @classmethod
    def setUpClass(cls):
        env = "sandbox"
        cls.config = load_config(PROJECT_ROOT, env)
        cls.handler = FilePersistenceHandler(get_transient_root(PROJECT_ROOT), cls.config.mode)

    def test_user_login(self):
        self.handler.delete_token(self.config.client.user_email)
        try:
            token = login_and_get_auth_token(self.config, self.handler)
            self.assertEqual(self.handler.get_token(self.config.client.user_email), token, "err-token-mismatch")
        except Exception as ex:
            traceback.print_exc()
            self.fail("err-failed-to-login-and-or-procure-auth-token")


if __name__ == '__main__':
    unittest.main()
