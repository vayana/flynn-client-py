import os
import unittest
from datetime import datetime

from FlynnClient.core.constants import EINVOICE_GSP_CODE, EINVOICE_GSTIN_HEADER, EINVOICE_PASSWORD_HEADER, \
    EINVOICE_USERNAME_HEADER, REQUEST_EKEY_HEADER
from FlynnClient.core.error import get_request_ekey
from FlynnClient.core.loader import load_json_file
from FlynnClient.utils.crypto import aes_encrypt, encrypt_with_key
from FlynnClient.utils.datetime import get_current_epoch
from FlynnClient.utils.string import str_to_bytes
from tests._core import BaseTest, PROJECT_ROOT


class TestGenerateIRNBulk(BaseTest):

    @classmethod
    def setUpClass(cls):
        super(TestGenerateIRNBulk, cls).setUpClass()
        file_path = os.path.join(PROJECT_ROOT, "config", cls.clientCtxt.env, "irp-user.json")
        cls.__irp_user = load_json_file(file_path)

    def __get_request_body(self):
        return {
            "payload": [
                {
                    "Version": "1.01",
                    "TranDtls": {
                        "TaxSch": "GST",
                        "SupTyp": "DEXP",
                        "RegRev": "N"
                    },
                    "DocDtls": {
                        "Typ": "INV",
                        "No": f"EASTC-{get_current_epoch()}",
                        "Dt": datetime.now().strftime('%d/%m/%Y')  # today's date
                    },
                    "SellerDtls": {
                        "Gstin": "27AAAPI3182M002",
                        "LglNm": "Acme Widgets Private Limited",
                        "Addr1": "2345",
                        "Loc": "Uttar Pradesh",
                        "Pin": 400049,
                        "Stcd": "27"
                    },
                    "BuyerDtls": {
                        "Gstin": "24AAAPI3182M002",
                        "LglNm": "Long Term Enterprises LLP",
                        "Pos": "24",
                        "Addr1": "1234",
                        "Loc": "Pune",
                        "Pin": 382170,
                        "Stcd": "24"
                    },
                    "ValDtls": {
                        "AssVal": 2500000,
                        "TotInvVal": 2950000,
                        "SgstVal": 0,
                        "CgstVal": 0,
                        "IgstVal": 450000
                    },
                    "DispDtls": {
                        "Nm": "Acme Widgets Private Limited",
                        "Addr1": "112",
                        "Addr2": "Acme Building",
                        "Loc": "ABC",
                        "Pin": 400049,
                        "Stcd": "27"
                    },
                    "ShipDtls": {
                        "Gstin": "27AAAPI3182M002",
                        "LglNm": "Acme Widgets Private Limited",
                        "TrdNm": "Acme Widgets Private Limited",
                        "Addr1": "112",
                        "Addr2": "Acme Building",
                        "Loc": "ABC",
                        "Pin": 400049,
                        "Stcd": "27"
                    },
                    "PayDtls": {
                        "Nm": "Acme Widgets Private Limited",
                        "Mode": "Cash",
                        "PayTerm": "100",
                        "PayInstr": "100",
                        "CrTrn": "100",
                        "DirDr": "100",
                        "CrDay": 100,
                        "PaymtDue": 900,
                        "PaidAmt": 100
                    },
                    "RefDtls": {
                        "InvRm": "Remarks",
                        "DocPerdDtls": {
                            "InvStDt": "01/07/2020",
                            "InvEndDt": "01/07/2020"
                        }
                    },
                    "ExpDtls": {
                        "ShipBNo": "shpbill123",
                        "ShipBDt": "01/07/2020",
                        "Port": "INV123",
                        "RefClm": "N",
                        "ForCur": "INR",
                        "CntCode": "IN",
                        "ExpDuty": 0
                    },
                    "EwbDtls": {
                        "TransId": "27AAAPI3182M002",
                        "TransName": "Driver",
                        "TransMode": "1",
                        "Distance": 5,
                        "TransDocNo": "20/22",
                        "VehNo": "KA51ES1122",
                        "VehType": "O"
                    },
                    "ItemList": [
                        {
                            "ItemNo": 1,
                            "SlNo": "1",
                            "PrdDesc": "Acme product",
                            "IsServc": "N",
                            "HsnCd": "1001",
                            "UnitPrice": 2500000,
                            "TotAmt": 2500000,
                            "AssAmt": 2500000,
                            "GstRt": 18,
                            "TotItemVal": 2950000,
                            "Qty": 1,
                            "Unit": "BAG",
                            "Discount": 0,
                            "SgstAmt": 0,
                            "CgstAmt": 0,
                            "IgstAmt": 450000,
                            "CgstRt": 0,
                            "SgstRt": 0,
                            "IgstRt": 0
                        }
                    ]
                }
            ],
            "meta": {
                "generatePdf": "Y",
                "emailRecipientList": [
                    self.clientCtxt.config.client.user_email
                ]
            }
        }

    def __call_generate_irn_enriched_api(self) -> str:
        flynn_version = "v1.0"
        path = f"enriched/einv/{flynn_version}/nic/invoices"

        request_body = self.__get_request_body()

        raw_rek = get_request_ekey()
        bytes_rek = str_to_bytes(raw_rek)
        e_rek = encrypt_with_key(self.clientCtxt.config.api_server_pk, bytes_rek)

        gstin = self.__irp_user["gstin"]
        username = self.__irp_user["username"]
        pwd = self.__irp_user["password"]
        e_pwd = aes_encrypt(bytes_rek, pwd)

        headers = {
            REQUEST_EKEY_HEADER: e_rek,
            EINVOICE_GSTIN_HEADER: gstin,
            EINVOICE_USERNAME_HEADER: username,
            EINVOICE_PASSWORD_HEADER: e_pwd,
            EINVOICE_GSP_CODE: "clayfin"
        }

        response = self.clientCtxt.client.post(path, {}, headers, request_body)
        self.assertIsNotNone(response, "err-call-to-server-failed")

        response_data = response.json()
        self.assertIsInstance(response_data, dict)

        if response.status_code == 200:
            task_reference = response_data["data"]["task-id"]
        else:
            self.fail(response_data['error']['message'])

        return task_reference

    def test_bulk_irn_generation(self):
        # initially we call the actual API
        # we will get async task id back
        task_id = self.__call_generate_irn_enriched_api()

        self._wait_for_task_to_complete(task_id)  # implements `status` API
        file_list = self._call_download_api(task_id)
        self.assertTrue(len(file_list) != 0)
        return


if __name__ == '__main__':
    unittest.main()
