import io
import json
import os
import unittest
import zipfile
from time import sleep
from typing import Any, BinaryIO, Dict, List, Optional, TextIO, Union

from FlynnClient.client import FlynnClient
from FlynnClient.core.constants import DEFAULT_SLEEP_TIME, MAX_ATTEMPTS
from FlynnClient.core.loader import get_client_root, get_transient_root, init_logger, load_config
from FlynnClient.handler import FilePersistenceHandler
from FlynnClient.utils.serialization import print_pretty

PROJECT_ROOT = get_client_root()
logger = init_logger(PROJECT_ROOT, "flynn.client")


class ClientContext:

    def __init__(self, env: str):
        config = load_config(PROJECT_ROOT, env)
        client = FlynnClient(config, FilePersistenceHandler(get_transient_root(PROJECT_ROOT), config.mode))

        self.env = env
        self.config = config
        self.client = client
        self.download_directory = os.path.join(PROJECT_ROOT, ".transient")


class BaseTest(unittest.TestCase):
    clientCtxt: ClientContext = None

    @classmethod
    def setUpClass(cls):
        env = os.environ.get("ENVIRONMENT", "sandbox")
        cls.clientCtxt = ClientContext(env)

    @classmethod
    def tearDownClass(cls):
        if cls.clientCtxt is not None:
            cls.clientCtxt.client.close()

    def __call_status_api(self, task_id: str) -> bool:
        """
        this method will call `status` API
        method will return `true` if completed else `false`
        """

        # waiting for async task to finish; hence sleeping
        sleep(DEFAULT_SLEEP_TIME)

        flynn_version = "v1.0"
        response = self.clientCtxt.client.get(f"/enriched/tasks/{flynn_version}/status/{task_id}")
        self.assertIsNotNone(response, "err-call-to-server-failed")

        response_data = response.json()
        self.assertIsInstance(response_data, dict)

        if response.status_code == 200:
            data = response_data["data"]
            self.assertEqual(task_id, data["id"])
            is_completed = data["status"] == "completed" and data["jobs"]["pending"] == 0
        else:
            self.fail(f"Status API Failed: {response_data['error']['message']}")

        return is_completed

    def _call_download_api(self, task_id: str) -> List[Union[TextIO, BinaryIO]]:
        """
        this method will call `download` API
        and it will save files which was found in response in the downloads directory
        """

        flynn_version = "v1.0"
        task_download_directory = os.path.join(self.clientCtxt.download_directory, "tasks", task_id)
        response = self.clientCtxt.client.get(f"/enriched/tasks/{flynn_version}/download/{task_id}", headers = {
            "accept": "application/zip"
        })
        self.assertIsNotNone(response, "err-call-to-server-failed")

        file_list = []
        if response.status_code == 200:
            if not os.path.exists(task_download_directory):
                os.makedirs(task_download_directory)

            # will get zipped response from the server
            zf = zipfile.ZipFile(io.BytesIO(response.content), "r")

            # parsing and writing files to the disk
            for file_info in zf.infolist():
                file_path = os.path.join(task_download_directory, file_info.orig_filename)

                if file_info.orig_filename.endswith('json'):
                    file_content = zf.read(file_info).decode('ascii')
                    file = open(file_path, "w")
                else:
                    file_content = zf.read(file_info)
                    file = open(file_path, "wb")

                file.write(file_content)
                file.close()
                file_list.append(file)
        else:
            response_data = response.json()
            self.fail(f"Download API Failed: {response_data['error']['message']}")

        return file_list

    def _wait_for_task_to_complete(self, task_id: str):
        """
         this is implementation of `status` API
         read more about long running tasks here,
         https://docs.enriched-api.vayana.com/routes/enriched/long-running-tasks/
        """

        # checking if task is completed or not
        completed = self.__call_status_api(task_id)
        attempts = 1
        while not completed:
            logger.debug(f"*** attempt #{attempts} ***")
            sleep(DEFAULT_SLEEP_TIME)
            completed = self.__call_status_api(task_id)

            if attempts == MAX_ATTEMPTS:
                self.fail("err-max-attempts-failed-for-status-api")

            attempts += 1
        return

    def _call_result_api(self, task_id: str):
        """
        this method will call `result` API
        and it will fetch json response from the server for the task-id
        """

        flynn_version = "v1.0"
        response = self.clientCtxt.client.get(f"/enriched/tasks/{flynn_version}/result/{task_id}")
        self.assertIsNotNone(response, "err-call-to-server-failed")

        if response.text:
            response_data = response.json()
            self.assertIsInstance(response_data, dict)

            if response.status_code == 200:
                data = response_data["data"]
            else:
                self.fail(response_data['error']['message'])
        else:
            self.fail("err-invalid-response")

        return data

    def _parse_json_response(self, response) -> Optional[Dict[str, Any]]:
        self.assertIsNotNone(response, "err-call-to-server-failed")

        if response.status_code == 504:
            self.fail("err-gateway-time-out")

        response_data = response.json()
        self.assertIsInstance(response_data, dict)

        if response.status_code != 200:
            self.fail(response_data['error']['message'])

        success_result = json.loads(response_data['data'])
        print_pretty(success_result)
        return success_result
