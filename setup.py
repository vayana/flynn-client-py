# !/usr/bin/env python

#
# Copyright (c) 2020 Vayana Network Services Pvt. Ltd. (VNSPL)
# All rights reserved.
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements. See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership. The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License. You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied. See the License for the
# specific language governing permissions and limitations
# under the License.
#

from __future__ import print_function

from os import path

from setuptools import find_namespace_packages, setup

here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'README.md'), encoding = 'utf-8') as f:
    long_description = f.read()

with open(path.join(here, 'src', 'version'), encoding = 'utf-8') as f:
    version = f.read()

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name = 'FlynnClient',
    version = version,
    description = 'Client Library to communicate to VNSPL\'s Enriched API Services (EAS)',
    license = 'Apache License 2.0',
    long_description = long_description,
    long_description_content_type = 'text/markdown',
    url = 'https://bitbucket.org/vayana/flynn-client-py',
    author = 'VNSPL Developers',
    author_email = 'eas-dev@vayana.com',
    python_requires = '>=3.6, <4',
    package_dir = {"": "src"},
    packages = find_namespace_packages(where = "src"),
    install_requires = required
)
