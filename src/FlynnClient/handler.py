import hashlib
import os
from typing import Optional

from FlynnClient.core.config import APIMode


class PersistenceHandler:

    def get_token(self, user_id: str) -> Optional[str]:
        raise NotImplementedError("err-not-implemented")

    def save_token(self, user_id: str, token: str):
        raise NotImplementedError("err-not-implemented")

    def delete_token(self, user_id: str) -> Optional[str]:
        raise NotImplementedError("err-not-implemented")


class FilePersistenceHandler(PersistenceHandler):

    def __init__(self, download_directory: str, api_mode: APIMode):
        self._api_mode = api_mode
        self.__download_directory = download_directory

    def __get_token_file_path(self, user_id: str):
        _id = f"{user_id}::{self._api_mode.value}"
        sha = hashlib.sha1(_id.encode())

        directory = os.path.join(self.__download_directory, "tokens")
        if not os.path.exists(directory):
            os.makedirs(directory, exist_ok = True)

        return os.path.join(directory, sha.hexdigest())

    def get_token(self, user_id: str) -> Optional[str]:
        token_file_path = self.__get_token_file_path(user_id)
        token = None
        if os.path.exists(token_file_path):
            with open(token_file_path, 'r') as f:
                token = f.read()

        return token

    def save_token(self, user_id: str, token: str):
        token_file_path = self.__get_token_file_path(user_id)
        with open(token_file_path, 'w') as f:
            f.write(token)
        return

    def delete_token(self, user_id: str):
        token_file_path = self.__get_token_file_path(user_id)
        os.remove(token_file_path)
        return
