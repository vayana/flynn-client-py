from typing import Tuple

from Crypto.Cipher import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.PublicKey.RSA import RsaKey
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.backends.interfaces import CipherBackend, X509Backend
from cryptography.hazmat.primitives.asymmetric.rsa import RSAPublicKey
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.hashes import SHA1
from cryptography.hazmat.primitives.padding import PKCS7

from FlynnClient.utils.string import b64_str_to_bytes, bytes_to_b64_str, bytes_to_str, str_to_bytes


##
# RSA
##


def load_rsa_key(key_file_path: str) -> Tuple[RsaKey, str]:
    with open(key_file_path, 'r') as f:
        key_str = f.read()
        key = RSA.importKey(key_str)
    return key, key_str


def load_public_key(key_file_path: str, backend: X509Backend) -> RSAPublicKey:
    with open(key_file_path, 'rb') as cert_file:
        cert_file_contents = cert_file.read()

    cert = x509.load_pem_x509_certificate(cert_file_contents, backend)
    return cert.public_key()


# encrypt

def _encrypt_with_key(key: RsaKey, data_bytes: bytes) -> bytes:
    # noinspection PyTypeChecker
    encryptor = PKCS1_v1_5.new(key)
    return encryptor.encrypt(data_bytes)


def encrypt_with_key(key: RsaKey, data_bytes: bytes) -> str:
    encrypted_bytes = _encrypt_with_key(key, data_bytes)
    return bytes_to_b64_str(encrypted_bytes)


# decrypt

def _decrypt_with_key(key: RsaKey, data_bytes: bytes) -> bytes:
    # noinspection PyTypeChecker
    decryptor = PKCS1_v1_5.new(key)
    # return decryptor.decrypt(ast.literal_eval(str(data_bytes)))
    return decryptor.decrypt(data_bytes)


def decrypt_with_key(key: RsaKey, data: str) -> str:
    data_in_bytes = b64_str_to_bytes(data)
    decrypted_bytes = _decrypt_with_key(key, data_in_bytes)
    return bytes_to_str(decrypted_bytes)


def sign(key: RsaKey, data_bytes: bytes) -> bytes:
    data_hash = SHA1.new(data_bytes)

    # noinspection PyTypeChecker
    signer = PKCS1_v1_5.new(key)

    signed_bytes = signer.sign(data_hash)
    return signed_bytes


def verify(key: RsaKey, data_bytes: bytes, signed_bytes: bytes) -> bool:
    data_hash = SHA1.new(data_bytes)

    # noinspection PyTypeChecker
    verifier = PKCS1_v1_5.new(key)

    try:
        verifier.verify(data_hash, signed_bytes)
        return True
    except ValueError:
        return False


##
# AES
##

# encrypt

def _aes_encrypt(bytes_key: bytes, data: bytes, backend: CipherBackend) -> bytes:
    cipher = Cipher(algorithms.AES(bytes_key), modes.ECB(), backend = backend)
    encryptor = cipher.encryptor()
    padder = PKCS7(128).padder()
    padded_bytes = padder.update(data) + padder.finalize()
    encrypted_bytes = encryptor.update(padded_bytes) + encryptor.finalize()
    return encrypted_bytes


def aes_encrypt(bytes_key: bytes, data: str, backend: CipherBackend = default_backend()) -> str:
    bytes_data = str_to_bytes(data)
    encrypted_bytes = _aes_encrypt(bytes_key, bytes_data, backend)
    return bytes_to_b64_str(encrypted_bytes)


# decrypt

def _aes_decrypt(bytes_key: bytes, data: bytes, backend: CipherBackend) -> bytes:
    cipher = Cipher(algorithms.AES(bytes_key), modes.ECB(), backend = backend)
    decryptor = cipher.decryptor()
    decrypted_bytes = decryptor.update(data) + decryptor.finalize()
    unpadder = PKCS7(128).unpadder()
    unpadded_bytes = unpadder.update(decrypted_bytes) + unpadder.finalize()
    return unpadded_bytes


def aes_decrypt(bytes_key: bytes, data: str, backend: CipherBackend = default_backend()) -> str:
    bytes_data = b64_str_to_bytes(data)
    decrypted_bytes_data = _aes_decrypt(bytes_key, bytes_data, backend)
    return bytes_to_str(decrypted_bytes_data)
