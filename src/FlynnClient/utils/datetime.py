from datetime import date, datetime, tzinfo
from typing import Optional

import pytz as pytz

TZ_IST = pytz.timezone('Asia/Kolkata')
TZ_UTC = pytz.utc


def get_current_utc_datetime() -> datetime:
    return datetime.utcnow().replace(tzinfo = TZ_UTC)


def get_current_epoch() -> int:
    return get_epoch_from_datetime(get_current_utc_datetime())


def get_current_date(tz: tzinfo) -> date:
    return get_current_utc_datetime().astimezone(tz).date()


def get_epoch_from_datetime(_datetime: datetime) -> int:
    return int(_datetime.astimezone(TZ_UTC).timestamp())


def get_epoch_from_date(_date: date) -> int:
    _datetime = datetime(_date.year, _date.month, _date.day, tzinfo = TZ_UTC)
    return get_epoch_from_datetime(_datetime)


def get_datetime_from_epoch(timestamp: int, tz: Optional[tzinfo] = None) -> datetime:
    result = datetime.fromtimestamp(timestamp, TZ_UTC)
    if tz:
        return result.astimezone(tz)
    return result


def get_date_from_epoch(timestamp: int, tz: Optional[tzinfo] = None) -> date:
    return get_datetime_from_epoch(timestamp, tz).date()


def get_epoch_ms_from_datetime(_datetime: datetime) -> int:
    return int(_datetime.astimezone(TZ_UTC).timestamp() * 1000)


def get_epoch_ms_from_date(_date: date) -> int:
    _datetime = datetime(_date.year, _date.month, _date.day, tzinfo = TZ_UTC)
    return get_epoch_ms_from_datetime(_datetime)


def get_datetime_from_epoch_ms(timestamp: int, tz: Optional[tzinfo] = None) -> datetime:
    return get_datetime_from_epoch(int(timestamp / 1000), tz)


def get_date_from_epoch_ms(timestamp: int, tz: Optional[tzinfo] = None) -> date:
    return get_date_from_epoch(int(timestamp / 1000), tz)
