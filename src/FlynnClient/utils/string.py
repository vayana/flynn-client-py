import base64


def str_to_bytes(data: str) -> bytes:
    return data.encode('utf-8')


def bytes_to_str(bytes_data: bytes) -> str:
    return bytes_data.decode('utf-8')


def bytes_to_b64_bytes(bytes_data: bytes) -> bytes:
    return base64.b64encode(bytes_data)


def b64_bytes_to_bytes(b64_bytes_data: bytes) -> bytes:
    return base64.b64decode(b64_bytes_data)


def b64_bytes_to_str(b64_bytes_data: bytes) -> str:
    return bytes_to_str(b64_bytes_to_bytes(b64_bytes_data))


def str_to_b64_bytes(str_data: str) -> bytes:
    return bytes_to_b64_bytes(str_to_bytes(str_data))


def str_to_b64_str(str_data: str) -> str:
    return bytes_to_str(bytes_to_b64_bytes(str_to_bytes(str_data)))


def b64_str_to_str(b64_str_data: str) -> str:
    return bytes_to_str(b64_str_to_bytes(b64_str_data))


def bytes_to_b64_str(bytes_data: bytes) -> str:
    return bytes_to_str(bytes_to_b64_bytes(bytes_data))


def b64_str_to_bytes(b64_str_data: str) -> bytes:
    return b64_bytes_to_bytes(str_to_bytes(b64_str_data))
