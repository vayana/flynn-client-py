import json
from datetime import date, datetime, time
from decimal import Decimal
from enum import Enum
from typing import Any

from FlynnClient.utils.datetime import get_epoch_from_date, get_epoch_from_datetime
from FlynnClient.utils.string import bytes_to_str


def print_pretty(data: Any) -> None:
    print(json.dumps(data, default = lambda o: object_to_dict(o), sort_keys = True, indent = 2))
    return


def object_to_dict(obj: Any):
    if isinstance(obj, (bool, int, float, str,)) or isinstance(obj, complex) or obj is None:
        return obj

    elif isinstance(obj, bytes):
        return bytes_to_str(obj)

    elif isinstance(obj, Decimal):
        return float(str(obj))

    elif isinstance(obj, Enum):
        return obj.value

    elif isinstance(obj, datetime):
        return get_epoch_from_datetime(obj)

    elif isinstance(obj, date):
        return get_epoch_from_date(obj)

    elif isinstance(obj, time):
        # Always return the time in the same timezone as that passed in
        # This means we'll always disregard the tzinfo.
        return obj.hour * 3600 + obj.minute * 60 + obj.second

    elif isinstance(obj, (set, list, tuple)):
        _list = []
        for item in obj:
            _list.append(object_to_dict(item))
        return _list

    elif isinstance(obj, dict):
        _dict = {}
        for key in obj:
            key_str = object_to_dict(key)
            _dict[key_str] = object_to_dict(obj[key])
        return _dict

    else:
        _dict = {}
        if hasattr(obj, '__dict__'):
            for key in obj.__dict__:
                _dict[key] = object_to_dict(obj.__dict__[key])
        return _dict
