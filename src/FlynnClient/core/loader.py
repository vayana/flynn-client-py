import json
import logging
import os
import sys
from typing import Any, Optional

from FlynnClient.core.config import FCConfig
from FlynnClient.core.error import FCError, FCErrorType
from FlynnClient.utils.datetime import TZ_IST, get_current_date


def get_client_root():
    current_path, filename = os.path.split(os.path.abspath(__file__))
    components = current_path.split(os.sep)

    # noinspection PyTypeChecker
    return str.join(os.sep, components[:components.index("flynn-client-py") + 1])


def get_config_root(project_root: str):
    return os.path.join(project_root, 'config')


def get_log_root(project_root: str):
    return os.path.join(project_root, 'logs')


def get_transient_root(project_root: str):
    return os.path.join(project_root, '.transient')


def __init_folders(project_root: str):
    for _dir in [get_log_root(project_root)]:
        if not os.path.exists(_dir):
            os.makedirs(_dir, 0o775)
    return


def load_json_file(file_path: str) -> Any:
    try:
        with open(file_path, 'r') as file:
            data = json.load(file)
    except FileNotFoundError as ex:
        raise FCError(ex, FCErrorType.System, "err-unable-to-locate-json-file", {"json_file_path": file_path})
    except Exception as ex:
        raise FCError(ex, FCErrorType.System, "err-unable-to-load-json", {"json_file_path": file_path})
    return data


def load_config(project_root: str, env: str):
    config_dir = get_config_root(project_root)
    file_path = os.path.join(config_dir, env, "client-config.json")
    config_data = load_json_file(file_path)
    __init_folders(project_root)
    config = FCConfig(project_root, **config_data)
    return config


def init_logger(project_root: str, logger_name: Optional[str] = None, log_level = logging.DEBUG,
        formatter: logging.Formatter = None, write_to_file: bool = True, write_to_console: bool = True):
    logger = logging.getLogger(logger_name or "flynn")
    logger.setLevel(log_level)

    if formatter is None:
        _fmt = '%(asctime)s - %(name)s - %(levelname)s - %(filename)s - %(lineno)s - %(funcName)s - %(message)s'
        formatter = logging.Formatter(_fmt)

    if write_to_file:
        # create file handler which logs even debug messages
        fh = logging.FileHandler(f'{get_log_root(project_root)}/{get_current_date(TZ_IST)}.log')
        fh.setLevel(log_level)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    if write_to_console:
        # create console handler with a higher log level
        ch = logging.StreamHandler(sys.stdout)
        ch.setLevel(log_level)
        ch.setFormatter(formatter)
        logger.addHandler(ch)

    return logger
