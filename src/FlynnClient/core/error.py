import random
import string
from enum import Enum
from typing import Any, Dict, Optional

from FlynnClient.utils.serialization import print_pretty


class FlynnClientErrorType(Enum):
    Runtime = 100
    System = 200
    Client = 300
    Api = 400


FCErrorType = FlynnClientErrorType


class FlynnClientError(Exception):

    def __init__(self, ex: Optional[Exception], code: FCErrorType = None, message: str = None,
            params: Dict[str, Any] = None):
        self.exception = ex
        self.code = code or FCErrorType.Runtime
        self.message = message or str(ex)
        self.params = params or {}
        super().__init__(self.message)

    def __str__(self):
        output = f'{self.message}\n'
        output += "Code: {}\n".format(self.code)
        if self.params:
            output += "Params: {}\n".format("; ".join("{}={}".format(k, v) for k, v in self.params.items()))
        return output

    @property
    def data(self):
        return {'code': self.code, 'message': self.message, 'param': self.params}

    def print(self):
        print_pretty(self.data)


FCError = FlynnClientError


def get_request_ekey(length: int = 32) -> str:
    if length not in (32, 64, 128):
        raise FCError(None, FCErrorType.Client, "err-invalid-key-length", {
            'length': length
        })

    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(length))
