ORG_IDENTIFIER_HEADER = 'X-FLYNN-N-ORG-ID'
USER_TOKEN_HEADER = 'X-FLYNN-N-USER-TOKEN'
REQUEST_EKEY_HEADER = 'X-FLYNN-S-REK'

EINVOICE_GSTIN_HEADER = 'X-FLYNN-N-IRP-GSTIN'
EINVOICE_USERNAME_HEADER = 'X-FLYNN-N-IRP-USERNAME'
EINVOICE_PASSWORD_HEADER = 'X-FLYNN-S-IRP-PWD'
EINVOICE_GSP_CODE = 'X-FLYNN-N-IRP-GSP-CODE'

DEFAULT_SLEEP_TIME = 15  # in seconds
MAX_ATTEMPTS = 6
SEP = "\n" + "*" * 80 + "\n"
