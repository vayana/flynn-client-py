import os
from enum import Enum
from typing import Optional, Tuple

from Crypto.PublicKey.RSA import RsaKey

from FlynnClient.core.error import FCError, FCErrorType
from FlynnClient.utils.crypto import load_rsa_key


class APIMode(Enum):
    Testing = 0
    Sandbox = 1
    Production = 2

    __dict__ = {
        Testing: 0,
        Sandbox: 1,
        Production: 2
    }


def _detect_mode(is_sandbox: Optional[bool], testing_url: str):
    if is_sandbox is None:
        return APIMode.Testing, testing_url

    mode = (APIMode.Sandbox if is_sandbox else APIMode.Production)
    url = f'https://{"solo" if is_sandbox else "live"}.enriched-api.vayana.com'
    return mode, url


def _load_public_key(project_root: str, key_file_path: Optional[str], default_key_name: str) -> Tuple[RsaKey, str]:
    if not key_file_path:
        key_file_path = os.path.join(project_root, "assets", default_key_name)

    if not os.path.exists(key_file_path):
        raise FCError(None, FCErrorType.Client, "err-auth-server-key-missing", {'path': key_file_path})

    return load_rsa_key(key_file_path)


class TheoClient:

    def __init__(self, **kwargs):
        self.org_id = kwargs["org_id"]
        self.user_email = kwargs["org_user"]["email"]
        self.user_pwd = kwargs["org_user"]["password"]


class EInvoiceUser:

    def __init__(self, **kwargs):
        self.gstin = kwargs["gstin"]
        self.user_name = kwargs["username"]
        self.user_pwd = kwargs["password"]


class FlynnClientConfig:

    def __init__(self, project_root: str, **kwargs):
        self.mode, self.base_url = _detect_mode(kwargs.get('is_sandbox'), kwargs.get('testing_url'))

        self.auth_server_pk, self.auth_server_pk_str = _load_public_key(project_root,
            kwargs.get('auth_server_public_key_file_path'), "theo-public-key.pem")

        self.api_server_pk, self.api_server_pk_str = _load_public_key(project_root,
            kwargs.get('api_server_public_key_file_path'), "flynn-public-key.pem")

        self.irp_server_pk, self.irp_server_pk_str = _load_public_key(project_root,
            kwargs.get('irp_public_key_file_path'), "irp-public-key.pem")

        self.client = TheoClient(**kwargs["client_details"])

        self.project_root = project_root
        self.download_directory = os.path.join(project_root, ".transient")
        if not os.path.exists(self.download_directory):
            os.mkdir(self.download_directory)


FCConfig = FlynnClientConfig
