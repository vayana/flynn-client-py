import functools
import json
import logging

from requests import Response

from FlynnClient.core.error import FCError, FCErrorType

logger = logging.getLogger("flynn.client")


class APIProcessor:

    def __init__(self, path: str = None, path_idx: int = None, path_key: str = "path"):
        if not any((path, path_idx, path_key)):
            raise FCError(None, FCErrorType.System, 'err-no-path-param-set')

        self.__path = path
        self.__path_idx = path_idx
        self.__path_key = path_key

    def __call__(self, func):
        @functools.wraps(func)
        def wrapper(this, *args, **kwargs):
            path = self.__path if self.__path else (kwargs.get(self.__path_key) or args[self.__path_idx])
            try:
                logger.debug(json.dumps({"message": "calling api", "func": func.__name__, "path": path}))
                response = func(this, *args, **kwargs)  # type: Response
                logger.debug(json.dumps({
                    "message": "api called", "status": response.status_code, "payload": response.text}))

                return response
            except Exception as ex:
                logger.debug(f"***ERROR CALLING FLYNN API***\n{ex}")

                if isinstance(ex, FCError):
                    logger.error(ex.message)
                else:
                    logger.error("err-call-to-flynn-server-failed", ex)
                return None

        return wrapper
