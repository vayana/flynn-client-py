import json
import logging

import jwt
import requests

from FlynnClient.core.config import APIMode, FCConfig
from FlynnClient.core.error import FCError, FCErrorType
from FlynnClient.handler import PersistenceHandler

logger = logging.getLogger("flynn.client")


def __get_base_url(config: FCConfig):
    if config.mode is APIMode.Production:
        return 'https://services.vayana.com/theodore/apis'

    return 'https://sandbox.services.vayananet.com/theodore/apis'


def login_and_get_auth_token(config: FCConfig, handler: PersistenceHandler) -> str:
    """
    This method here tries to read the saved (on the local-disk) auth-token;
    is not found it will try to login and extract token from response
    """

    user_id = config.client.user_email
    token = handler.get_token(user_id)
    if token:
        try:
            payload = jwt.decode(token, config.auth_server_pk_str, algorithms = ['RS256'])

            # checking org-user relationship; user has to be primary member of the org
            is_valid_org = False
            for org_str in payload['orgs']:
                org = json.loads(org_str)
                if org["oid"] == config.client.org_id and org["prim"]:
                    is_valid_org = True
                    break

            if not is_valid_org:
                raise FCError(None, FCErrorType.Runtime, "err-user-org-relationship-invalid")

        except jwt.exceptions.ExpiredSignatureError:
            handler.delete_token(user_id)
            token = None

    if token is None:
        # using v1 __version__ of the API - of the auth server
        url = f'{__get_base_url(config)}/v1/authtokens'
        logger.debug(f"calling url: {url}")

        response = requests.post(url, json = {
            'handle': user_id,
            'password': config.client.user_pwd,
            'handleType': "email"
        })

        if response.status_code == 200:
            response_body = response.json()
            logger.debug(f"response data: {response.text}")
            token = response_body['data']['token']
            handler.save_token(config.client.user_email, token)

        else:
            raise FCError(None, FCErrorType.Runtime, "err-login-to-server-failed", {
                "response": {
                    "status-code": response.status_code,
                    "content": response.content
                }})
    return token
