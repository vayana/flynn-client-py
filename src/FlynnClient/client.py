import json
import logging
from typing import Any, Dict, Optional

import requests

from FlynnClient.auth import login_and_get_auth_token
from FlynnClient.core.config import FCConfig
from FlynnClient.core.constants import ORG_IDENTIFIER_HEADER, REQUEST_EKEY_HEADER, USER_TOKEN_HEADER
from FlynnClient.core.error import FCError, FCErrorType
from FlynnClient.core.processor import APIProcessor
from FlynnClient.handler import PersistenceHandler
from FlynnClient.utils.crypto import aes_encrypt, encrypt_with_key
from FlynnClient.utils.datetime import get_current_epoch
from FlynnClient.utils.serialization import object_to_dict
from FlynnClient.utils.string import str_to_bytes

logger = logging.getLogger("flynn.client")


class FlynnClient:

    def __init__(self, config: FCConfig, handler: PersistenceHandler):
        self.session = requests.Session()
        self._config = config
        self._handler = handler

    # def get_task_download_directory(self, task_id: str):
    #     dir_path = os.path.join(self._config.download_directory, "tasks", task_id)
    #     if not os.path.exists(dir_path):
    #         os.makedirs(dir_path, exist_ok = True)
    #     return dir_path

    def __authenticate_and_get_default_headers(self):
        return {
            'Content-Type': 'application/json',
            'accept': 'application/json',
            ORG_IDENTIFIER_HEADER: self._config.client.org_id,
            USER_TOKEN_HEADER: login_and_get_auth_token(self._config, self._handler)
        }

    def _call(self, method: str, url: str, query_params: Dict[str, str], headers: Dict[str, str], body: Any = None):
        _header = headers.copy()
        _header.update({ORG_IDENTIFIER_HEADER: "<masked>", USER_TOKEN_HEADER: "<masked>"})

        api_params = {
            'method': method,
            'url': url,
            'query_params': query_params,
            'headers': _header,
            'body': body
        }

        start_time = get_current_epoch()

        try:
            if method == 'GET':
                response = self.session.get(url, params = query_params, headers = headers)
            else:
                if isinstance(body, dict):
                    headers['Content-Type'] = 'application/json'

                    if method == 'POST':
                        response = self.session.post(url, json = body, headers = headers)
                    elif method == 'PUT':
                        response = self.session.put(url, json = body, headers = headers)
                    elif method == 'PATCH':
                        response = self.session.patch(url, json = body, headers = headers)
                    else:
                        raise FCError(None, FCErrorType.Client, 'err-unhandled-method', {'method': method})

                else:
                    raise FCError(None, FCErrorType.Client, 'err-only-json-object-payload-is-supported')

            api_call_failure_status = False
        except Exception as e:
            raise FCError(e, FCErrorType.Api, "err-api-call-failed", {'api_params': api_params})

        api_params['response'] = {
            "http_status_code": response.status_code,
            "content": response.text,
            "x-call-id": response.headers.get("X-Call-Id")
        }

        api_params['time_taken'] = get_current_epoch() - start_time

        try:
            logger.debug(json.dumps(object_to_dict(api_params)))
        except UnicodeDecodeError as cause:
            logger.debug(f'can\'t decode. non unicode chars in response. {cause}')

        return response

    def _call_ppp(self, method: str, path: str, fields: Dict[str, str], headers: Dict[str, str], body: Any):
        if headers is None:
            headers = {}

        headers.update(self.__authenticate_and_get_default_headers())
        url = f'{self._config.base_url}/{path}'
        return self._call(method, url, fields, headers, body)

    @APIProcessor(path_idx = 0)
    def post(self, path: str, fields: Optional[Dict[str, str]] = None, headers: Optional[Dict[str, str]] = None,
            body: Optional[Any] = None):
        return self._call_ppp("POST", path, fields, headers, body)

    @APIProcessor(path_idx = 0)
    def put(self, path: str, fields: Optional[Dict[str, str]] = None, headers: Optional[Dict[str, str]] = None,
            body: Optional[Any] = None):
        return self._call_ppp("PUT", path, fields, headers, body)

    @APIProcessor(path_idx = 0)
    def patch(self, path: str, fields: Optional[Dict[str, str]] = None, headers: Optional[Dict[str, str]] = None,
            body: Optional[Any] = None):
        return self._call_ppp("PATCH", path, fields, headers, body)

    @APIProcessor(path_idx = 0)
    def get(self, path: str, fields: Dict[str, str] = None, headers: Dict[str, str] = None):
        if fields is None:
            fields = {}

        if headers is None:
            headers = {}

        headers.update(self.__authenticate_and_get_default_headers())

        url = f'{self._config.base_url}{path}'
        return self._call('GET', url, fields, headers)

    @APIProcessor(path = "/ping")
    def ping(self):
        url = f'{self._config.base_url}/ping'
        return self._call('GET', url, {}, headers = {'Content-Type': 'application/json'})

    @APIProcessor(path = "/authenticate")
    def authenticate(self):
        url = f'{self._config.base_url}/authenticate'
        return self._call('GET', url, {}, self.__authenticate_and_get_default_headers())

    @APIProcessor(path = "/authenticate-with-secrets")
    def authentication_with_sensitive_data(self, rek: str, client_message: str):
        url = f'{self._config.base_url}/authenticate-with-secrets'

        # generating REK and encrypting with api-server public key
        bytes_rek = str_to_bytes(rek)
        _ekey = encrypt_with_key(self._config.api_server_pk, bytes_rek)

        # encrypting secret message with `REK`
        encrypted_secret_message = aes_encrypt(bytes_rek, client_message)

        headers = self.__authenticate_and_get_default_headers()
        headers[REQUEST_EKEY_HEADER] = _ekey
        headers['X-FLYNN-S-DATA'] = encrypted_secret_message
        return self._call('GET', url, {}, headers)

    def close(self):
        self.session.close()
