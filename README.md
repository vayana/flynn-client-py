# Enriched API (Flynn) Client

This project helps to consume Enriched API Services provided by Vayana Network Services Pvt. Ltd. (VNSPL).

Client library intends to add only all various ping calls,

- `/ping`: a health check call

- `/authenticate`: which uses default header; based on the authentication server (`theodore`) 
    
    - you can check docs for the auth server [here](https://docs-auth.vayananet.com/)

- `/authenticate-with-secrets`: which uses extra headers (`X-FLYNN-S-REK` and `X-FLYNN-S-DATA`); 
this is to test - sharing of sensitive data to the server
    
    - `X-FLYNN-S-REK` is the 'Request Encryption Key' (either 32, 64 or 128 char string). 
    This key is a dynamic salt for AES encryption of the sensitive data sent either in the request body or in the header.
    The key itself is encrypted with RSA key shared by the server. 
    Note: the `.pem` shared with you are different for production and sandbox environments.
    
    - `X-FLYNN-S-DATA` is the any sensitive data you want to send it to the server.
    This sensitive data is AES encrypted via 'Request Encrypted Key'.
    To test the encryption-decryption of the sensitive data shared between the client and the server - server is sending the original message (AES encrypted) in the response.
    Where encryption key is still the same what was sent in the request header.

You can follow following links for detailed documentation of the Enriched API Services [here](https://docs.enriched-api.vayana.com/)

## How to use the project?

### Install `requirements`

1. Create a virtual env with Python version 3.7+

2. install requirements using `requirements.txt`

```bash
pip install -r requirements.txt
```

### Configure

1. copy from `sample.json` (from the config folder) and create your config.

2. to get the instance of the client, you need to pass this config.

3. also make sure you have all the required files to create the instance for the client.
    - Enriched-API Server's (Flynn) issued Public Key
    - Authentication Server's (Theodore) issued Public Key
    
